%global _empty_manifest_terminate_build 0
%global pypi_name tox-pdm 

Name:           python-%{pypi_name}
Version:        0.7.2
Release:        1
Summary:        A plugin for tox that utilizes PDM as the package manager and installer.

License:        MIT
URL:            https://github.com/pdm-project/tox-pdm
Source0:        https://files.pythonhosted.org/packages/source/t/tox_pdm/tox_pdm-%{version}.tar.gz 
BuildArch:      noarch

BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
BuildRequires:	python3-hatchling
BuildRequires:  python3-pdm-pep517
BuildRequires:  python3-pdm-backend

%description
A plugin for tox that utilizes PDM as the package manager and installer.


%package -n     python3-%{pypi_name}
Summary:        %{summary}

%description -n python3-%{pypi_name}
A plugin for tox that utilizes PDM as the package manager and installer.


%prep
%autosetup -p1 -n tox_pdm-%{version}

%build
%pyproject_build

%install
%pyproject_install

%files -n python3-%{pypi_name}
%doc README.md
%license LICENSE
%{python3_sitelib}/tox_pdm
%{python3_sitelib}/tox_pdm*.dist-info/

%changelog
* Mon May 20 2024 dongjiao <dongjiao@kylinos.cn> - 0.7.2-1
- Update to 0.7.2
  - set tox minimum version to 4.0
 
* Sat Jun 24 2023 Dongxing Wang <dxwangk@isoftstone.com> - 0.6.1-1
- Initial package 0.6.1
